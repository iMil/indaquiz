#!/usr/bin/env python

import json
import os
import re
import time

import smtplib
import mimetypes
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from flask import Flask, render_template, request, session, url_for
app = Flask(__name__)

app.config['TEMPLATES_AUTO_RELOAD'] = True
app.secret_key = "I just want to use sessions."

quizzes = "quizzes"
keyfile = "keys.txt"
poweruser = "root"

def send_result(quizname):
    with open("mail.json") as f:
        j = json.load(f)

    msg = MIMEMultipart()
    msg["From"] = j["from"]
    msg["To"] = j["mailto"]
    msg["Subject"] = "{0} - {1} - {2}".format(
        session["key"], session["timestamp"], quizname
    )

    with open(session["csvfile"]) as f:
        msg.attach(MIMEText(f.read()))

    srv = smtplib.SMTP(j["smtp_server"])
    if "login" in j and "password" in j:
        srv.starttls()
        srv.login(j["login"], j["password"])
    srv.sendmail(msg["From"], msg["To"], msg.as_string())
    srv.quit()


def write_result(str):
    with open(session["csvfile"], "a") as f:
        f.write(u"{0}\n".format(str).encode('utf8'))


def delete_key():
    if not "key" in session or session["key"] == None:
        return
    with open(keyfile) as f:
        kf = f.readlines()
    with open(keyfile, "w") as f:
        for l in kf:
            if not l.startswith(session["key"]) or session["key"] == poweruser:
                f.write(l)


def endgame(score):
    delete_key()
    session.pop("key", None)
    session.pop("user_points", None)
    session.pop("actual_points", None)
    session.pop("timestamp", None)
    session.pop("test", None)

    return render_template("end.html", score = score)


def record_cheat():
    session["cheat"] = session["cheat"] + 1
    session["cheat_alert"] = True
    return session["cheat"]


@app.route("/quiz/<quizname>", methods=["POST", "GET"])
@app.route("/quiz/<quizname>/<q>", methods=["POST", "GET"])
def quiz(quizname, q = -1):

    with open("{0}/{1}.json".format(quizzes, quizname)) as f:
        j = json.load(f)

    q = int(q)  # question number
    nq = len(j)  # total number of questions
    print(q, session)

    # GET comes from index or subhome, otherwhise it's cheating
    if request.method == "GET":
        if q < 0:
            session["user_points"] = 0
            session["actual_points"] = 0
            session["key"] = None
            session["cheat"] = 0
            session["previous"] = -1
            session["cheat_alert"] = False
            return render_template("subhome.html", nq = nq, quizname = quizname)
        else:
            if record_cheat() > 1:
                return endgame(0)

    if request.method == "POST":

        # warning has been shown previously, mute it
        if session["cheat_alert"] is True:
            session["cheat_alert"] = False
        if "previous" in session and session["previous"] == q:
            if record_cheat() > 1:
                return endgame(0)

        # record question number for reload check
        session["previous"] = q

        # key check while trying to load 1st quiz quesion
        if 'key' in request.form:
            with open(keyfile) as f:
                lines = f.read().splitlines()
            for l in lines:
                if l.startswith(request.form["key"]):
                    session["key"], session["test"] = [x.strip() for x in l.split(":")]

            if not "key" in session or session["key"] == None or not re.compile(session["test"]).search(quizname):
                return render_template("subhome.html", nq = nq, quizname = quizname)
            else:
                session["timestamp"] = time.strftime("%d_%m_%Y")
                session["csvfile"] = "results/{0}-{1}-{2}.csv".format(
                    session["key"],
                    quizname,
                    session["timestamp"]
                )
                q = 0
        # answer check
        else:
            if not "key" in session:
                return render_template("subhome.html", nq = nq, quizname = quizname)

            checkarr = []  # record checked items
            result = []  # candidate answers
            idx = q - 1 # question index, -1 as we're processing the query
            # extract given answers from request.form
            for k, v in request.form.items():
                checkarr.append(int(v) - 1)

            result = {
                "question": [j[idx]["q"]],
                "choices": [],
                "user_answers": [],
                "correct_answers": [],
                "user_points": [],
                "actual_points": []
            }
            # build comprehensive result array
            result["choices"] = [x["c"].keys()[0] for x in j[idx]["a"]]
            result["correct_answers"] = [x["c"].values()[0] for x in j[idx]["a"]]
            result["actual_points"] = [x["points"] for x in j[idx]["a"]]

            for i, a in enumerate(j[idx]["a"]):
                # has the user checked this number ?
                if i in checkarr:
                    r = True
                else:
                    r = False
                result["user_answers"].append(r)
                # fill question column for printing purposes
                result["question"].append("")
                # fill the user_points
                if r is True and j[idx]["a"][i]["c"].values()[0] is True:
                    result["user_points"].append(j[idx]["a"][i]["points"])
                else:
                    result["user_points"].append(0)

            write_result("{0},{1},{2},{3},{4},{5}".format(
                "Question",
                "Choices",
                "Correct Answer",
                "User Answer",
                "User's Points",
                "All Points by choice"
            ))

            for i in range(len(result["choices"])):
                write_result(u'"{0}","{1}",{2},{3},{4},{5}'.format(
                    result["question"][i],
                    result["choices"][i],
                    result["correct_answers"][i],
                    result["user_answers"][i],
                    result["user_points"][i],
                    result["actual_points"][i]
                ))

            session["user_points"] = session["user_points"] + sum(result["user_points"])
            session["actual_points"] = session["actual_points"] + sum(result["actual_points"])


    if q + 1 > nq:
        write_result(",,,,{0},{1}".format(session["user_points"], session["actual_points"]))
        send_result(quizname)
        return endgame(session["user_points"])

    return render_template(
        "quiz.html", j = j[q], q = q, nq = nq, quizname = quizname
    )


@app.route("/")
def home():
    return render_template("home.html", cats = os.listdir(quizzes))
