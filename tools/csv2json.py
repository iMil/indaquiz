#!/usr/bin/env python

from __future__ import print_function

import csv
import json
import sys

csvfile = sys.argv[1]

fields = ("Question", "Multi", "Time", "Choices", "Correct", "Points", "Position")

f = open(csvfile, "r")

def s2b(val):
    if val:
        if val.startswith("T"):
            return True
        elif val.startswith("F"):
            return False
        else:
            print("Incorrect boolean value", file=sys.stderr)
            sys.exit(2)
quiz = []
jsonfile = None
qn = -1
for i, row in enumerate(csv.DictReader(f, fields)):
    if not i:
        continue

    if row["Position"]:
        jsonfile = "quizzes/{0}.json".format(row["Position"])
    for e in ["Correct", "Multi"]:
        row[e] = s2b(row[e])
    if row["Question"]:
        quiz.append(
            {
                "q": row["Question"],
                "a": [
                    {
                        "c": {
                            row["Choices"]: row["Correct"],
                        },
                        "points": int(row["Points"])
                    }
                ],
                "multi": row["Multi"],
                "timer": row["Time"]
            }
        )
        qn = qn + 1
    else:
        quiz[qn]["a"].append(
            {
                "c": {
                    row["Choices"]: row["Correct"],
                },
                "points": int(row["Points"])
            }
        )

f.close()

with open(jsonfile, "w") as f:
    json.dump(quiz, f, indent=2)
